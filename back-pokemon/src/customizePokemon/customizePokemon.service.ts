import { Injectable, HttpException, HttpStatus, HttpCode } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import {Repository, UpdateResult} from "typeorm";
import { CustomizePokemon } from "./customizePokemon.entity";
import { CreateCustomizePokemonDto } from "./dto/createCustomizePokemon.dto";
import { PokemonService } from "../pokemon/pokemon.service";
import { CapacityService } from "../capacity/capacity.service";
import {UpdateCustomizePokemonDto} from "./dto/updateCustomizePokemon.dto";

@Injectable()
export class CustomizePokemonService {
    constructor(
        @InjectRepository(CustomizePokemon)
        private readonly customizePokemonRepository: Repository<CustomizePokemon>,
        private readonly pokemonService: PokemonService,
        private readonly capacityService: CapacityService
    ){}
    findAll(): Promise<CustomizePokemon[]> {
        return this.customizePokemonRepository.find();
    }

    async findOne(id: number): Promise<any> {
        return this.customizePokemonRepository.createQueryBuilder('customizePokemon')
            .where("customizePokemon.id = :id",{ id})
            .leftJoinAndSelect('customizePokemon.idTypePokemon', 'pokemonType')
            .leftJoinAndSelect('customizePokemon.idCapacity1', 'capacity1')
                .leftJoinAndSelect('capacity1.idTypePokemon', 'type1')
            .leftJoinAndSelect('customizePokemon.idCapacity2', 'capacity2')
                .leftJoinAndSelect('capacity2.idTypePokemon', 'type2')
            .getOne();
    }
    
   async create(customizePokemonDto: CreateCustomizePokemonDto): Promise<any> {
       var message;
       var status;
       var res;
       const capacity1 = await this.capacityService.findByName('Pound');
       const capacity2 = await this.capacityService.findByName('Double Slap');
        await this.pokemonService.findOne(customizePokemonDto.idPokemon).then(async pokemon => {
            var customPokemon = new CustomizePokemon();
            customPokemon.name = pokemon.name;
            customPokemon.hp = pokemon.hp;
            customPokemon.pointAttack = pokemon.pointAttack;
            customPokemon.pointDefense = pokemon.pointDefense;
            customPokemon.speed = pokemon.speed;
            customPokemon.idCapacity1 = capacity1.id;
            customPokemon.idCapacity2 = capacity2.id;
            customPokemon.totalPoint = pokemon.totalPoint;
            customPokemon.idTypePokemon = pokemon.idTypePokemon.id;
            customPokemon.numero = pokemon.numero;
           await this.capacityService.findOne(capacity1.id).then( async capacity1 => {
              await  this.capacityService.findOne(capacity2.id).then(async capacity2 => {
                    var customPoint = (customPokemon.pointAttack + customPokemon.pointDefense + customPokemon.speed );
                    if (pokemon.totalPoint < customPoint ){
                        message = "Le dopage est interdit ici !";
                        status = 3
                    }
                    else{
                    await this.verifCapacityWithTypePokemon(capacity1.idTypePokemon['id'], capacity2.idTypePokemon['id'], pokemon.idTypePokemon.id)
                        .then(async result => {
                        if(result == true){
                            message = "Profite bien de ton pokemon";
                            status = 1;
                            res = await this.customizePokemonRepository.save(customPokemon)
                        }
                        else{
                            message = "Les types de capacités ne sont pas adapter !";
                            status = 2
                        }
                    })      
                 }
                }).catch(error => console.log(error))
            }).catch(error => console.log(error))
        }).catch(error => console.log(error));
       if (status == 1){
           throw new HttpException({res, message} ,HttpStatus.CREATED)
       }
       if (status == 2){
          throw new HttpException( {message} ,HttpStatus.BAD_REQUEST)
       }
       if (status == 3){
           throw new HttpException( {message} ,HttpStatus.BAD_REQUEST)
       }
    }

    async update(id: number, updateCustomizePokemonDto: UpdateCustomizePokemonDto): Promise<UpdateResult> {
        return await this.customizePokemonRepository.update(id, updateCustomizePokemonDto);
    }

    async verifCapacityWithTypePokemon(capacity1 ,capacity2 ,typePokemon):Promise<any>{
        var result = false;
        //1 represente la capaciter normal qui peut etre attribuer a tout les pokemon
        if(( typePokemon == capacity1 ) && ( typePokemon == capacity2 )){
            result=true
        }
        if(( capacity1 == 1 )&&( typePokemon == capacity2 )){
            result= true
        }
        if(( typePokemon == capacity1 )&&( capacity2 == 1 )){
            result= true
        }
        if(( capacity1 == 1 )&&( capacity2 == 1 )){
            result= true
        }
        return result
    }

    async remove(id: number): Promise<void> {
        await this.customizePokemonRepository.delete(id);
    }
}
