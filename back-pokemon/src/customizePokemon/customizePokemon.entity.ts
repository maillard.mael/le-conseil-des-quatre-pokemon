import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany
} from "typeorm";
import { TypePokemon } from "../typePokemon/typePokemon.entity";
import { Capacity } from "../capacity/capacity.entity";
import {UserInventory} from "../userInventory/userInventory.entity";

@Entity()
export class CustomizePokemon {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    numero: number;
    @Column()
    name: string;
    @Column()
    hp: number;
    @Column()
    pointAttack: number;
    @Column()
    pointDefense: number;
    @Column()
    speed: number;
    @Column()
    totalPoint: number;
    @OneToMany('UserInventory', 'idCustomizePokemon', {eager: true})
    inventoryPokemon: UserInventory['idCustomizePokemon'];
    @ManyToOne(() => TypePokemon, typePokemon => typePokemon.id, {eager: true})
    @JoinColumn({name: 'typePokemon-1', referencedColumnName: 'id'})
    idTypePokemon: TypePokemon['id'];
    @ManyToOne(() => Capacity, capacity => capacity.id, {eager: true})
    @JoinColumn({name: 'capacity-1', referencedColumnName: 'id'})
    idCapacity1: Capacity['id'];
    @ManyToOne(() => Capacity, capacity => capacity.id, {eager: true})
    @JoinColumn({name: 'capacity-2', referencedColumnName: 'id'})
    idCapacity2: Capacity['id'];
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
}
