import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsInt, IsString } from "class-validator";
import { TypePokemon } from "../../typePokemon/typePokemon.entity";
import { Capacity } from "../../capacity/capacity.entity";
import { Pokemon } from "../../pokemon/pokemon.entity";

export class CreateCustomizePokemonDto {
    @ApiProperty()
    @IsNotEmpty()
    idPokemon: Pokemon['id'];
/*    @ApiProperty()
    @IsNotEmpty()
    idCapacity1: Capacity['id'];
    @ApiProperty()
    @IsNotEmpty()
    idCapacity2: Capacity['id'];*/
}
