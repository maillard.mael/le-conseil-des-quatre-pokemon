import { ApiProperty } from "@nestjs/swagger";
import {IsInt, IsNotEmpty, IsString} from "class-validator";
import {Capacity} from "../../capacity/capacity.entity";

export class UpdateCustomizePokemonDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    hp: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    pointAttack: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    pointDefense: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    speed: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    idCapacity1: Capacity['id'];
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    idCapacity2: Capacity['id'];
}
