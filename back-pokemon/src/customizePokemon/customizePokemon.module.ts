import { CustomizePokemon } from "./customizePokemon.entity";
import { CustomizePokemonService } from "./customizePokemon.service";
import { CustomizePokemonController } from "./customizePokemon.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from "@nestjs/common";
import { PokemonModule } from "../pokemon/pokemon.module";
import { CapacityModule } from "../capacity/capacity.module";

@Module({
    imports: [TypeOrmModule.forFeature([CustomizePokemon]),PokemonModule, CapacityModule], 
    providers: [CustomizePokemonService],
    controllers: [CustomizePokemonController],
    exports:[CustomizePokemonService]
})
export class CustomizePokemonModule {}