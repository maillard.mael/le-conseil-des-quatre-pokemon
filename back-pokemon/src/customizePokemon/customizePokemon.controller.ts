import { ApiTags } from "@nestjs/swagger";
import {Controller, Post, Body, Get, Param, NotFoundException, Delete, Put} from "@nestjs/common";
import { Pokemon } from "../pokemon/pokemon.entity";
import { CustomizePokemonService } from "./customizePokemon.service";
import { CreateCustomizePokemonDto } from "./dto/createCustomizePokemon.dto";
import {CustomizePokemon} from "./customizePokemon.entity";
import {UpdateCustomizePokemonDto} from "./dto/updateCustomizePokemon.dto";

@ApiTags('customizePokemon')
@Controller('customizePokemon')
export class CustomizePokemonController {
    constructor(private readonly customizePokemonService: CustomizePokemonService) {}

    @Get()
    async findAll(): Promise<Pokemon[]> {
        return this.customizePokemonService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id: number): Promise<CustomizePokemon> {
        const customizePokemon = await this.customizePokemonService.findOne(id);
        if (customizePokemon === undefined) {
            throw new NotFoundException('Custom Pokemon not found')
        }
        return customizePokemon;
    }

    @Post()
    async create(@Body() customizePokemonDto: CreateCustomizePokemonDto): Promise<Pokemon> {
        return  await this.customizePokemonService.create(customizePokemonDto);
    }

    @Put(':id')
    async update(@Param('id') id: number, @Body() updateCustomizePokemonDto: UpdateCustomizePokemonDto): Promise<any> {
        const pokemon = await this.customizePokemonService.findOne(id);
        if (pokemon === undefined) {
            throw new NotFoundException('CustomPokemon not found')
        } else {
            return await this.customizePokemonService.update(id ,updateCustomizePokemonDto);
        }
    }

    @Delete(':id')
    async remove(@Param('id') id: number): Promise<void> {
        return this.customizePokemonService.remove(id);
    }
}
