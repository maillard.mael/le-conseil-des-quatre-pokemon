import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import {  IsPseudoUserExist } from './user.validator';
import {UserInventoryModule} from "../userInventory/userInventory.module";
@Module({

imports: [TypeOrmModule.forFeature([User]), UserInventoryModule],
providers: [UserService, IsPseudoUserExist ],
controllers: [UserController],
exports: [UserService],
})
export class UserModule{}
