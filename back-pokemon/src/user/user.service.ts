import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from "./user.entity";
import { Injectable } from "@nestjs/common";
import { UserDto } from './user.dto';
import * as bcrypt from 'bcryptjs';
import { UserInventory } from "../userInventory/userInventory.entity";

@Injectable()
export class UserService {
    private saltRounds = 10;
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) { }

    async findByPseudo(pseudo: string): Promise<User> {
        const user: User = await this.userRepository.createQueryBuilder('user')
            .where('user.pseudo like :pseudo', { pseudo })
            .leftJoinAndSelect('user.userInventory', 'inventory')
            .leftJoinAndSelect('inventory.idCustomizePokemon', 'pokemon')
            .leftJoinAndSelect('pokemon.idTypePokemon', 'pokemonType')
            .leftJoinAndSelect('pokemon.idCapacity1', 'capacity1')
            .leftJoinAndSelect('pokemon.idCapacity2', 'capacity2')
            .getOne();
        return user;
    }
    
    async findById(id: string): Promise<User> {
        const user: User = await this.userRepository.createQueryBuilder('user')
            .where('user.id = :id', { id })            
            .leftJoinAndSelect('user.userInventory', 'inventory')
            .leftJoinAndSelect('inventory.idCustomizePokemon', 'pokemon')
            .leftJoinAndSelect('pokemon.idTypePokemon', 'pokemonType')
            .leftJoinAndSelect('pokemon.idCapacity1', 'capacity1')
            .leftJoinAndSelect('pokemon.idCapacity2', 'capacity2')
            .leftJoinAndSelect('user.historyOne', 'historyOne')
            .leftJoinAndSelect('user.historyTwo', 'historyTwo')
            // .leftJoinAndMapMany('historic.idUserOne', 'historic')
            .getOne();
        return user;
    }

    async findAll(): Promise<User[]> {
        return this.userRepository.find();
    }
    async create(createUserDto: UserDto): Promise<any> {
        const sql = await this.userRepository.createQueryBuilder()
            .insert()
            .into(User)
            .values([
                { pseudo: createUserDto.pseudo, password: await this.getHash(createUserDto.password) }
            ])
            .execute();
        return sql;
    }

    async getHash(password: string | undefined): Promise<string> {
        return bcrypt.hash(password, this.saltRounds);
    }
    async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }


    async findPokemonByUserId(userId: number): Promise<User> {
        return await this.userRepository.createQueryBuilder('user')
            .where('user.id = :userId', { userId })
            .leftJoinAndSelect('user.userInventory', 'inventory')
            .leftJoinAndSelect('inventory.idCustomizePokemon', 'pokemon')
            .leftJoinAndSelect('pokemon.idTypePokemon', 'pokemonType')
            .leftJoinAndSelect('pokemon.idCapacity1', 'capacity1')
            .leftJoinAndSelect('capacity1.idTypePokemon', 'type1')
            .leftJoinAndSelect('pokemon.idCapacity2', 'capacity2')
            .leftJoinAndSelect('capacity2.idTypePokemon', 'type2')
            .getOne();
    }
}
