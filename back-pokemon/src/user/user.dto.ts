import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { IsPseudoUserValidator } from './user.validator';

export class UserDto {
    @IsNotEmpty()
    @IsString()
    @IsPseudoUserValidator({message: 'pseudo déja utilisé'})
    @ApiProperty()
    readonly pseudo: string;
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    readonly password: string;
}