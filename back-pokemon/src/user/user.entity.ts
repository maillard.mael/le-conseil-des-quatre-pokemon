import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany} from 'typeorm';
import {UserInventory} from "../userInventory/userInventory.entity";
import { Historic } from 'src/historic/historic.entity';
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    pseudo:string;
    @Column()
    password:string;
    @OneToMany('UserInventory', 'idUser', {eager: true})
    userInventory: UserInventory['idUser'];
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany('Historic', 'idUserOne', {eager: true})
    historyOne: Historic['idUserOne'];
    @OneToMany('Historic', 'idUserTwo', {eager: true})
    historyTwo: Historic['idUserTwo'];

}
