import { UserService } from "./user.service";
import { Controller, Get, Post, Body, Param, UseGuards, Request, Req, NotFoundException } from "@nestjs/common";
import { User } from "./user.entity";
import { ApiTags, ApiResponse } from '@nestjs/swagger';
import { UserDto } from "./user.dto";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";

@ApiTags('user')
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    @ApiResponse({ status: 200, description: 'User Found.' })
    @ApiResponse({ status: 404, description: 'No Users found.' })
    @Get('me')
    @UseGuards(JwtAuthGuard)
    async findUser(@Req() req) {
        const user = await this.userService.findById(req.user.userId)
        if (user === undefined) {
            throw new NotFoundException('User not found')
        }
        return user;
    }

    @Get()
    async findAll(): Promise<User[]> {
        return this.userService.findAll();
    }


    @Post()
    async create(@Body() createUserDto: UserDto) {
        return this.userService.create(createUserDto);
    }

    @ApiResponse({ status: 404, description: 'No Users found.' })
    @Get('inventory')
    @UseGuards(JwtAuthGuard)
    async findInventory(@Req() req): Promise<User> {        
        const user = await this.userService.findById(req.user.userId)
        return this.userService.findPokemonByUserId(user.id);
    }
}
