import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments, registerDecorator, ValidationOptions } from "class-validator";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "./user.entity";
import { Repository } from "typeorm";
import { UserService } from "./user.service";

@ValidatorConstraint({ async: true })
@Injectable()
export class IsPseudoUserExist implements ValidatorConstraintInterface {
    constructor(
    protected readonly userService: UserService,
   ) {}
    async validate(pseudo: string, args: ValidationArguments) {

      const user = await this.userService.findByPseudo(pseudo)
    
      if (user === undefined) {
            return true;
        }
      return false;
    }
 

}

export function IsPseudoUserValidator(validationOptions?: ValidationOptions) {
    return (object, propertyName: string)  => {
         registerDecorator({
             target: object.constructor,
             propertyName,
             options: validationOptions,
             constraints: [],
             validator: IsPseudoUserExist,
         });
    };
 }