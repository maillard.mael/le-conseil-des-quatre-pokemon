import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.modules';
import { AlertController } from './alert/alert.controller';
import { AlertGateway } from './alert/alert.gateway';
import { ChatGateway } from './chat/chat.gateway';
import { TypePokemonModule } from "./typePokemon/typePokemon.module";
import { PokemonModule } from "./pokemon/pokemon.module";
import { CapacityModule } from "./capacity/capacity.module";
import { CustomizePokemonModule } from './customizePokemon/customizePokemon.module';
import { UserInventoryModule } from "./userInventory/userInventory.module";
import { HistoricModule } from './historic/historic.module';
import { MasterServerGateway } from './master-server/master-server.gateway';
import { AuthService } from './auth/auth.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      // host: 'localhost',
      host: 'db',
      port: 5432,
      username: 'pkmn',
      password: 'pkmn',
      // username: 'postgres',
      // password: 'postgres',
      database: 'pokemon',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }), UserModule, AuthModule, TypePokemonModule, PokemonModule, CapacityModule, CustomizePokemonModule, UserInventoryModule, HistoricModule
  ],
  controllers: [AlertController],
  providers: [ChatGateway, AlertGateway, MasterServerGateway],
})
export class AppModule { }
