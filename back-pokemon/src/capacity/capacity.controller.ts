import {Body, Controller, Delete, Get, Param, Post, Put} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {CapacityService} from "./capacity.service";
import {Capacity} from "./capacity.entity";
import {CreateCapacityDto} from "./dto/createCapacity.dto";
import {UpdateCapacityDto} from "./dto/updateCapacity.dto";

@ApiTags('capacities')
@Controller('capacities')
export class CapacityController {
    constructor(private readonly capacityService: CapacityService) {}

    @Post()
    create(@Body() createCapacityDto: CreateCapacityDto): Promise<Capacity> {
        return this.capacityService.create(createCapacityDto);
    }

    @Get()
    findAll(): Promise<Capacity[]> {
        return this.capacityService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: number): Promise<Capacity> {
        return this.capacityService.findOne(id);
    }

    // @Put(':id')
    // async update(@Param('id') id: number, @Body() updateCapacityDto: UpdateCapacityDto): Promise<any> {
    //     return this.capacityService.update(id ,updateCapacityDto);
    // }

    @Delete(':id')
    remove(@Param('id') id: number): Promise<void> {
        return this.capacityService.remove(id);
    }
}
