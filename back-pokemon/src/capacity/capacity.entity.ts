import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {TypePokemon} from "../typePokemon/typePokemon.entity";

@Entity()
export class Capacity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @Column()
    puissance: number;
    @Column()
    precision: number;
    @ManyToOne(() => TypePokemon, typePokemon => typePokemon.id)
    @JoinColumn({name: 'typePokemon', referencedColumnName: 'id'})
    idTypePokemon: TypePokemon['id'];
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
}
