import {IsInt, IsNotEmpty, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {TypePokemon} from "../../typePokemon/typePokemon.entity";

export class UpdateCapacityDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    puissance: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    precision: number;
    @ApiProperty({type:String})
    @IsNotEmpty()
    idTypePokemon: TypePokemon['id'];
}
