import {Injectable} from "@nestjs/common";
import {InjectRepository} from '@nestjs/typeorm';
import {Repository, UpdateResult} from "typeorm";
import {Capacity} from "./capacity.entity";
import {CreateCapacityDto} from "./dto/createCapacity.dto";
import {UpdateCapacityDto} from "./dto/updateCapacity.dto";
import { TypePokemonService } from "../typePokemon/typePokemon.service";

@Injectable()
export class CapacityService {
    constructor(
        @InjectRepository(Capacity)
        private readonly capacityRepository: Repository<Capacity>,
        private readonly typePokemonService: TypePokemonService
    ) {}

    async findAll(): Promise<Capacity[]> {
        return this.capacityRepository.createQueryBuilder('capacity')
            .leftJoinAndSelect('capacity.idTypePokemon', 'type')
            .getMany()
    }

    async findOne(id: number): Promise<Capacity> {
        return this.capacityRepository.createQueryBuilder('capacity')
        .where('capacity.id = :id',{id})
            .leftJoinAndSelect('capacity.idTypePokemon', 'type')
        .getOne()
    }
    async findByName(name: string): Promise<Capacity>{
        return this.capacityRepository.createQueryBuilder('capacity')
        .where('capacity.name = :name',{name})
        .getOne()
    }

    async create(createCapacityDto: CreateCapacityDto): Promise<Capacity> {
        const capacity = new Capacity();
        capacity.name = createCapacityDto.name;
        capacity.puissance = createCapacityDto.puissance;
        capacity.precision = createCapacityDto.precision;
        capacity.idTypePokemon = createCapacityDto.idTypePokemon;

        return this.capacityRepository.save(capacity);
    }

    // async update(id: number, updateCapacityDto: UpdateCapacityDto): Promise<UpdateResult> {
    //     return await this.capacityRepository.update(id, updateCapacityDto);
    // }

    async remove(id: number): Promise<void> {
        await this.capacityRepository.delete(id);
    }

    createwithSeeder(): Array<Promise<any>>{
        const capacities = require('./capacity.json')
        return capacities.map( async capacity => {
            console.log(capacity)
            return await this.findByName(capacity.ename)
            .then( db =>{
                if (db){
                    return Promise.resolve(null);
                }
                this.typePokemonService.findByName(capacity.type).then(
                    typePokemon => {
                        console.log(typePokemon)
                        var newDto = new CreateCapacityDto()
                        newDto.name = capacity.ename
                        newDto.puissance = capacity.power
                        newDto.precision = capacity.accuracy
                        newDto.idTypePokemon = typePokemon.id

                        return Promise.resolve(
                            this.create(newDto).catch(error => console.log(error))
                        )
                    })
             }).catch(error => console.log(error))
        })
    }
}
