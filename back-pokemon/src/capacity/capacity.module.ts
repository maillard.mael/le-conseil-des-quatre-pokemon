import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Capacity} from "./capacity.entity";
import {CapacityService} from "./capacity.service";
import {CapacityController} from "./capacity.controller";
import { TypePokemonModule } from '../typePokemon/typePokemon.module';

@Module({
    imports: [TypeOrmModule.forFeature([Capacity]),TypePokemonModule],
    providers: [CapacityService],
    controllers: [CapacityController],
    exports:[CapacityService]
})
export class CapacityModule {}
