import {
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {User} from "../user/user.entity";
import {CustomizePokemon} from "../customizePokemon/customizePokemon.entity";

@Entity()
export class UserInventory {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne('User', 'userInventory', {eager: false})
    @JoinColumn({name: 'idUser', referencedColumnName: 'id'})
    idUser: User;
    @ManyToOne('CustomizePokemon', 'inventoryPokemon', {eager: false, onDelete: 'CASCADE'})
    @JoinColumn({name: 'idCustomPokemon', referencedColumnName: 'id'})
    idCustomizePokemon: CustomizePokemon;
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
}
