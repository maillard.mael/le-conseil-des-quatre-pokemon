import {User} from "../../user/user.entity";
import {IsNotEmpty} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
// import {CustomizePokemon} from "../../customizePokemon/customizePokemon.entity";
import {Pokemon} from "../../pokemon/pokemon.entity";

export class UpdateUserInventoryDto {
    @ApiProperty({type:String})
    @IsNotEmpty()
    idUser: User;
    @ApiProperty({type:String})
    @IsNotEmpty()
    idCustomizePokemon: Pokemon;
}
