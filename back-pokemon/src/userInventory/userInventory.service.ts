import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {UserInventory} from "./userInventory.entity";
import {Repository, UpdateResult} from "typeorm";
import {CreateUserInventoryDto} from "./dto/create-user-inventory.dto";
import {UpdateUserInventoryDto} from "./dto/update-user-inventory.dto";

@Injectable()
export class UserInventoryService {
    constructor(
        @InjectRepository(UserInventory)
        private readonly userInventoryRepository: Repository<UserInventory>,
    ) {}

    findAll(): Promise<UserInventory[]> {
        return this.userInventoryRepository.find();
    }

    async findByUserId(userId: string): Promise<UserInventory[]> {
        return await this.userInventoryRepository.createQueryBuilder('inventory')
            .where('inventory.idUser = :userId', {userId})
            .leftJoinAndSelect('inventory.idCustomPokemon', 'pokemons')
            .getMany();
    }

    findOne(id: string): Promise<UserInventory> {
        return this.userInventoryRepository.findOne(id);
    }

    create(createUserInventoryDto: CreateUserInventoryDto): Promise<UserInventory> {
        return this.userInventoryRepository.save(createUserInventoryDto);
    }

    async update(id: string, updateUserInventoryDto: UpdateUserInventoryDto): Promise<UpdateResult> {
        return await this.userInventoryRepository.update(id, updateUserInventoryDto);
    }

    async remove(id: string): Promise<void> {
        await this.userInventoryRepository.delete(id);
    }
}
