import {ApiTags} from "@nestjs/swagger";
import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put} from "@nestjs/common";
import {UserInventoryService} from "./userInventory.service";
import {CreateUserInventoryDto} from "./dto/create-user-inventory.dto";
import {UserInventory} from "./userInventory.entity";
import {UpdateUserInventoryDto} from "./dto/update-user-inventory.dto";

@ApiTags('userInventories')
@Controller('userInventories')
export class UserInventoryController {
    constructor(private readonly userInventoryService: UserInventoryService) {}

    @Post()
    create(@Body() createUserInventoryDto: CreateUserInventoryDto): Promise<UserInventory> {
        return this.userInventoryService.create(createUserInventoryDto);
    }

    @Get()
    findAll(): Promise<UserInventory[]> {
        return this.userInventoryService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<UserInventory> {
        const userInventory = await this.userInventoryService.findOne(id);
        if (userInventory === undefined) {
            throw new NotFoundException('UserInventory not found')
        }
        return userInventory;
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() updateUserInventoryDto: UpdateUserInventoryDto): Promise<any> {
        const userInventory = await this.userInventoryService.findOne(id);
        if (userInventory === undefined) {
            throw new NotFoundException('UserInventory not found')
        } else {
            return await this.userInventoryService.update(id ,updateUserInventoryDto);
        }
    }

    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.userInventoryService.remove(id);
    }
}
