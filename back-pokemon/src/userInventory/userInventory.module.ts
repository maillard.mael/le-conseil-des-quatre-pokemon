import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UserInventory} from "./userInventory.entity";
import {UserInventoryService} from "./userInventory.service";
import {UserInventoryController} from "./userInventory.controller";

@Module({
    imports: [TypeOrmModule.forFeature([UserInventory])],
    providers: [UserInventoryService],
    controllers: [UserInventoryController],
    exports: [UserInventoryService]
})
export class UserInventoryModule {}
