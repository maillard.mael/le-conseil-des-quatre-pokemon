import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {TypePokemon} from "./typePokemon.entity";
import {TypePokemonService} from "./typePokemon.service";
import {TypePokemonController} from "./typePokemon.controller";

@Module({
    imports: [TypeOrmModule.forFeature([TypePokemon])],
    providers: [TypePokemonService],
    controllers: [TypePokemonController],
    exports: [TypePokemonService]
})
export class TypePokemonModule {}
