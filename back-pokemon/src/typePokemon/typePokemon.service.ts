import {Injectable} from "@nestjs/common";
import {InjectRepository} from '@nestjs/typeorm';
import {TypePokemon} from "./typePokemon.entity";
import {Repository} from "typeorm";
import {CreateTypePokemonDto} from "./dto/create-typePokemon.dto";

@Injectable()
export class TypePokemonService {
    constructor(
        @InjectRepository(TypePokemon)
        protected typePokemonRepository: Repository<TypePokemon>,
    ) {}

    findAll(): Promise<TypePokemon[]> {
        return this.typePokemonRepository.find();
    }

    findOne(id: string): Promise<TypePokemon> {
        return this.typePokemonRepository.findOne(id);
    }

    findByName(name: string): Promise<TypePokemon>{
        return this.typePokemonRepository.createQueryBuilder('typePokemon')
        .where('typePokemon.name = :name',{name})
        .getOne()
    }

    findCapacitiesByType(id: string): Promise<TypePokemon>{
        return this.typePokemonRepository.createQueryBuilder('typePokemon')
            .where('typePokemon.id = :id',{id})
            .leftJoinAndSelect('typePokemon.capacities', 'capacities')
            .getOne()
    }

    create(createTypePokemonDto: CreateTypePokemonDto): Promise<TypePokemon> {
        const typePokemon = new TypePokemon();
        typePokemon.name = createTypePokemonDto.name;

        return this.typePokemonRepository.save(typePokemon);
    }

    createwithSeeder(): Array<Promise<any>>{
        const types = require('./typePokemon.json')
        return types.map( async type => {
            console.log(type)
            return await this.findByName(type.english)
            .then( db =>{
                if (db){
                    return Promise.resolve(null);
                }
                var newDto = new CreateTypePokemonDto()
                newDto.name = type.english
                return Promise.resolve(
                    this.create(newDto).catch(error => console.log(error))
                )
            }).catch(error => console.log(error))
        })
    }
}
