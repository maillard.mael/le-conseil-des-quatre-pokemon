import {Body, Controller, Get, NotFoundException, Param, Post} from '@nestjs/common';
import {TypePokemonService} from "./typePokemon.service";
import {CreateTypePokemonDto} from "./dto/create-typePokemon.dto";
import {TypePokemon} from "./typePokemon.entity";
import {ApiTags} from "@nestjs/swagger";

@ApiTags('pokemonTypes')
@Controller('pokemonTypes')
export class TypePokemonController {
    constructor(private readonly typePokemonService: TypePokemonService) {}

    @Post()
    create(@Body() createTypePokemonDto: CreateTypePokemonDto): Promise<TypePokemon> {
        return this.typePokemonService.create(createTypePokemonDto);
    }

    @Get()
    findAll(): Promise<TypePokemon[]> {
        return this.typePokemonService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<TypePokemon> {
        const typePokemon = await this.typePokemonService.findOne(id);
        if (typePokemon === undefined) {
          throw new NotFoundException('Type not found')
        } return typePokemon;
    }

    @Get(':id/capacities')
    async findCapacities(@Param('id') id: string): Promise<TypePokemon> {
        const typePokemon = await this.typePokemonService.findCapacitiesByType(id);
        if (typePokemon === undefined) {
            throw new NotFoundException('Type not found')
        } return typePokemon;
    }
}
