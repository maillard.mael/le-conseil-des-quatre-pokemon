import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany} from 'typeorm';
import {Capacity} from "../capacity/capacity.entity";

@Entity()
export class TypePokemon {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name: string;
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
    @OneToMany('Capacity', 'idTypePokemon', {eager: true})
    capacities: Capacity['idTypePokemon'];
}
