import { Injectable, Logger } from "@nestjs/common";
import { TypePokemonService } from "./typePokemon/typePokemon.service";
import { PokemonService } from "./pokemon/pokemon.service";
import { Capacity } from "./capacity/capacity.entity";
import { CapacityService } from "./capacity/capacity.service";

@Injectable()
export class Seeder {
  constructor(
    private readonly logger: Logger,
    private readonly typePokemonService:TypePokemonService,
    private readonly pokemonService: PokemonService,
    private readonly capacityService: CapacityService

  ){}
  async seed() {
    await this.typesPokemon()
    .then(completed => {
      this.logger.debug('Successfuly completed seeding typePokemon...');
      Promise.resolve(completed);
    });
    await this.capacity()
        .then(completed => {
          this.logger.debug('Successfuly completed seeding capacities...');
          Promise.resolve(completed);
        });
    await this.pokemon()
    .then(completed => {
      this.logger.debug('Successfuly completed seeding pokemon...');        
      Promise.resolve(completed);
    })
  }


  async typesPokemon() {
    return await Promise.all(this.typePokemonService.createwithSeeder())
      .then(CreatedType => {
        this.logger.debug(
          'No. of types created : ' +
            CreatedType.filter(
              nullValueOrCreated => nullValueOrCreated,
            ).length,
      );
        return Promise.resolve(true);
      })
      .catch(error => Promise.reject(error));
  }
  async pokemon() {
    return await Promise.all(this.pokemonService.createwithSeeder())
      .then(CreatedPokemon => {
        this.logger.debug(
          'No. of Pokemon created : ' +
            CreatedPokemon.filter(
              nullValueOrCreated => nullValueOrCreated,
            ).length,
      );
        return Promise.resolve(true);
      })
      .catch(error => Promise.reject(error));
  }

  async capacity(){
    return await Promise.all(this.capacityService.createwithSeeder())
      .then(CreatedCapacity => {
        this.logger.debug(
          'No. of Capacities created : ' +
          CreatedCapacity.filter(
              nullValueOrCreated => nullValueOrCreated,
            ).length,
      );
        return Promise.resolve(true);
      })
      .catch(error => Promise.reject(error));
  }

}
