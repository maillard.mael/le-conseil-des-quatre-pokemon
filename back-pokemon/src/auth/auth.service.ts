import { Injectable } from "@nestjs/common";
import { UserService } from "../user/user.service";
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly usersService: UserService,
              private readonly jwtService: JwtService) {}

  async validateUser(pseudo: string, password: string): Promise<any> {
    const user = await this.usersService.findByPseudo(pseudo);
    if (user && await this.usersService.compareHash(password, user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { pseudo: user.pseudo, sub: user.id  };
    return {
      access_token: this.jwtService.sign(payload),
      id: user.id
    };
  }

  async getUserByToken(token: string): Promise<any>{
    return this.jwtService.decode(token.split(" ")[1])
  }

}