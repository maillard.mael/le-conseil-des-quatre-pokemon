import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
/*
    Custom imports for AuthService, jwt secret, etc...
*/
import * as jwt from 'jsonwebtoken';
import { AuthService } from './auth.service';
import passport = require('passport');
import { JwtStrategy } from './jwt.strategy';
import { jwtConstants } from './constants';

@Injectable()
export class WsJwtGuard implements CanActivate {
    constructor(private authService: AuthService) { }

    async canActivate(context: ExecutionContext) {
        const client = context.switchToWs().getClient();
        var jwttoken: any = client.handshake.headers.authorization
        jwttoken = jwttoken.split(' ')[1];
        var user = ""
        var jwt = require('jsonwebtoken');
        if (jwttoken === undefined) {
            throw new UnauthorizedException();
        } else {
            const user = jwt.verify(jwttoken, jwtConstants.secret, (err, result) => {
                if (err) {
                    return new UnauthorizedException();
                }
                return result;
            });
            if (user.pseudo === undefined) {
                new Error('authentication error');
                throw new UnauthorizedException();
            }

            return user
        }

    }
    handleRequest(err, user, info) {
        if (err || !user) {
            throw err || new UnauthorizedException();
        }
        return user;
    }
}