import { UserModule } from "../user/user.module";
import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { jwtConstants } from './constants';

import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from "./local.strategy";

@Module({
    imports: [UserModule, PassportModule, 
     JwtModule.register({
     secret: jwtConstants.secret,
     signOptions: { expiresIn: '9999 years' },
     }) ,
   ],
     providers: [AuthService, LocalStrategy, JwtStrategy],
     controllers: [AuthController],
     exports: [AuthService,JwtStrategy,PassportModule]
   })
   export class AuthModule {}