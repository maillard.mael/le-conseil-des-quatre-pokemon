import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";
import {TypePokemon} from "../typePokemon/typePokemon.entity";
import { Capacity } from "../capacity/capacity.entity";

@Entity()
export class Pokemon {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    numero: number;
    @Column()
    name: string;
    @Column()
    hp: number;
    @Column()
    pointAttack: number;
    @Column()
    pointDefense: number;
    @Column()
    speed: number;
    @ManyToOne(() => TypePokemon, typePokemon => typePokemon.id, {eager: true})
    @JoinColumn({name: 'typePokemon', referencedColumnName: 'id'})
    idTypePokemon: TypePokemon['id'];
    @Column()
    totalPoint: number;
    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;
}
