import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {PokemonService} from "./pokemon.service";
import {CreatePokemonDto} from "./dto/create-pokemon.dto";
import {Pokemon} from "./pokemon.entity";
import {UpdatePokemonDto} from "./dto/update-pokemon.dto";

@ApiTags('pokemons')
@Controller('pokemons')
export class PokemonController {
    constructor(private readonly pokemonService: PokemonService) {}

    @Post()
    async create(@Body() createPokemonDto: CreatePokemonDto): Promise<Pokemon> {
        return this.pokemonService.create(createPokemonDto);
    }

    @Get()
    async findAll(): Promise<Pokemon[]> {
        return this.pokemonService.findAll();
    }
    @Get('/random')
    async random(): Promise<Pokemon> {
         return  this.pokemonService.randomPokemon();
    }


    @Get(':id')
    async findOne(@Param('id') id: number): Promise<Pokemon> {
        const pokemon = await this.pokemonService.findOne(id);
        if (pokemon === undefined) {
            throw new NotFoundException('Pokemon not found')
        }
        return pokemon;
    }

    @Put(':id')
    async update(@Param('id') id: number, @Body() updatePokemonDto: UpdatePokemonDto): Promise<any> {
        const pokemon = await this.pokemonService.findOne(id);
        if (pokemon === undefined) {
            throw new NotFoundException('Pokemon not found')
        } else {
            return await this.pokemonService.update(id ,updatePokemonDto);
        }
    }

    @Delete(':id')
    async remove(@Param('id') id: number): Promise<void> {
        return this.pokemonService.remove(id);
    }
}
