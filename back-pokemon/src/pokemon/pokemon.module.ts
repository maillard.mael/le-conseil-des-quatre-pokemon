import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Pokemon} from "./pokemon.entity";
import {PokemonService} from "./pokemon.service";
import {PokemonController} from "./pokemon.controller";
import { TypePokemonModule } from '../typePokemon/typePokemon.module';

@Module({
    imports: [TypeOrmModule.forFeature([Pokemon]),TypePokemonModule],
    providers: [PokemonService],
    controllers: [PokemonController],
    exports:[PokemonService]
})
export class PokemonModule {}
