import {Injectable} from "@nestjs/common";
import {InjectRepository} from '@nestjs/typeorm';
import {Pokemon} from "./pokemon.entity";
import {Repository, UpdateResult} from "typeorm";
import {CreatePokemonDto} from "./dto/create-pokemon.dto";
import {UpdatePokemonDto} from "./dto/update-pokemon.dto";
import { TypePokemonService } from "../typePokemon/typePokemon.service";
import pokemonClassement from "./pokemonCumul.json";


@Injectable()
export class PokemonService {
    
    constructor(
        @InjectRepository(Pokemon)
        private readonly pokemonRepository: Repository<Pokemon>,
        private readonly typePokemonService: TypePokemonService
    ) {}
    

    async findAll(): Promise<Pokemon[]> {
        return this.pokemonRepository.find();
    }

    async findOne(id: number): Promise<any> {
        return this.pokemonRepository.createQueryBuilder('pokemon')
        .where("pokemon.id = :id",{ id})
            .leftJoinAndSelect('pokemon.idTypePokemon', 'type')
        .getOne();
    }
    async findByName(name: string): Promise<Pokemon>{
        return this.pokemonRepository.createQueryBuilder('pokemon')
        .where("pokemon.name = :name",{name})
        .getOne()
    }

    async create(createPokemonDto: CreatePokemonDto): Promise<Pokemon> {
        return this.pokemonRepository.save(createPokemonDto);
    }

    async update(id: number, updatePokemonDto: UpdatePokemonDto): Promise<UpdateResult> {
        return await this.pokemonRepository.update(id, updatePokemonDto);
    }

    async remove(id: number): Promise<void> {
        await this.pokemonRepository.delete(id);
    }

    async randomPokemon(): Promise<any>{
        function getRandomInt(max) {
            return Math.floor(Math.random() * Math.floor(max));
        }
       return this.pokemonRepository.createQueryBuilder('pokemon')
        .select("SUM(pokemon.totalPoint)", "totalPoint")
        .getRawOne()
        .then( async result => {
            var numberRandom = getRandomInt(result.totalPoint)
            const goodPokemon = pokemonClassement.filter(idPokemon => (numberRandom  >= idPokemon.intervalleMin && numberRandom <= idPokemon.intervalleMax))
             return this.findOne(goodPokemon[0].id).then( result2 => {
                        return result2
                     })  
              })
     }

    createwithSeeder(): Array<Promise<any>>{
        const pokemons = require('./pokedex.json')
        return pokemons.map( async pokemon => {
            if (pokemon.id > 649){
                return Promise.resolve(null);
            }
            // console.log(pokemon)
            return await this.findByName(pokemon.name.english)
            .then( db =>{
                if (db){
                    return Promise.resolve(null);
                }
                this.typePokemonService.findByName(pokemon.type[0]).then(
                    typePokemon => {
                        // console.log(typePokemon)
                        var newDto = new CreatePokemonDto()
                        newDto.name = pokemon.name.english
                        newDto.hp = pokemon.base.HP
                        newDto.pointAttack = pokemon.base.Attack
                        newDto.pointDefense = pokemon.base.Defense
                        newDto.numero = pokemon.id
                        newDto.speed = pokemon.base.Speed
                        newDto.totalPoint = pokemon.base.Speed + pokemon.base.Defense + pokemon.base.Attack
                        newDto.idTypePokemon  = typePokemon.id
                        return Promise.resolve(
                            this.create(newDto).catch(error => console.log(error))
                        )
                    }).catch(error => console.log(error))
            }).catch(error => console.log(error))
        })
    }
}
