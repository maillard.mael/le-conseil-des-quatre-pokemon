import {IsInt, IsNotEmpty, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";
import {TypePokemon} from "../../typePokemon/typePokemon.entity";
import { Capacity } from "../../capacity/capacity.entity";

export class CreatePokemonDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    numero: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    hp: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    pointAttack: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    pointDefense: number;
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    speed: number;
    @ApiProperty()
    @IsNotEmpty()
    idTypePokemon: TypePokemon['id'];
    @IsNotEmpty()
    @IsInt()
    totalPoint:number
}
