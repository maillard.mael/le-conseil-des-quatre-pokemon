import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer, OnGatewayConnection } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Logger, UseGuards, Req } from '@nestjs/common';
import { WsJwtGuard } from '../auth/jwt-ws';
import { Injectable } from "@nestjs/common";
import { AuthService } from 'src/auth/auth.service';
import { Pokemon } from 'src/pokemon/pokemon.entity';
import { Column } from 'typeorm';
import { HistoricService } from 'src/historic/historic.service';
import { CreateHistoricDto } from 'src/historic/dto/create-historic.dto';
import { User } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';

enum LobbyStatus {
  Waiting = "Waiting for players",
  Pending = "Game in progress"
}
enum TurnStatus {
  Waiting = "Waiting for players",
  Pending = "Game in progress"
}
enum Actions {
  Run = "Run",
  ChangePokemon = "ChangePokemon",
  Attack = "Attack"
}
enum ActionsPriority {
  Run,
  ChangePokemon,
  Attack
}
const MAX_PLAYERS = 2;

const BASE_ACTION = {
  type: null,
  priority: null,
  pokemon: null
}

const BASE_DATA = {
  dialog: [],
  lobby: null
}

const BASE_PLAYER = {
  data: JSON.parse(JSON.stringify(BASE_DATA)),
  name: null,
  socketId: null,
  pokemons: [],
  activePokemon: null,
  action: {
    type: null,
    priority: null,
    pokemon: null,
    attack: null
  }
};

const BASE_LOBBY = {
  players: [
    JSON.parse(JSON.stringify(BASE_PLAYER)),
    JSON.parse(JSON.stringify(BASE_PLAYER))
  ],
  status: LobbyStatus.Waiting
}

@Injectable()
@WebSocketGateway({ namespace: '/masterServer' })
export class MasterServerGateway implements OnGatewayInit {

  constructor(private readonly authService: AuthService, private readonly historicService: HistoricService, private readonly userService: UserService) { }

  private lobbies = [
    JSON.parse(JSON.stringify(BASE_LOBBY)),
    JSON.parse(JSON.stringify(BASE_LOBBY)),
    JSON.parse(JSON.stringify(BASE_LOBBY)),
    JSON.parse(JSON.stringify(BASE_LOBBY)),
    JSON.parse(JSON.stringify(BASE_LOBBY))
  ];

  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('MasterServerGateway');

  afterInit(server: any) {
    this.logger.log('Initialized!');
  }

  @SubscribeMessage('clientToMasterServer')
  handleMessage(client: Socket, message: { sender: string, lobby: string, message: string }) {
    this.wss.to(message.lobby).emit('masterServerToClient', message);
  }

  @SubscribeMessage('get-lobbies')
  getAllLobbies(client: Socket) {
    client.emit('lobbies', this.lobbies);
  }

  private canJoinLobby(lobby: string) {
    let slotAvailable = false
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == null) {
        slotAvailable = true
        break
      }
    }
    console.log(slotAvailable)
    return slotAvailable
  }

  private addPLayerToLobby(lobby: string, player: string, socketId: string) {
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == null) {
        this.lobbies[lobby].players[indexPlayer].name = player
        this.lobbies[lobby].players[indexPlayer].socketId = socketId
        break;
      }
    }
  }

  private removePLayerFromLobby(lobby: string, player: string) {
    for (let index = 0; index < this.lobbies[lobby].players.length; index++) {
      if (this.lobbies[lobby].players[index].name == player) {
        this.lobbies[lobby].players[index].name = null
        break;
      }
    }
  }

  @UseGuards(WsJwtGuard)
  @SubscribeMessage('join-lobby')
  async handleLobbyJoin(client: Socket, lobby: string) {
    if (this.canJoinLobby(lobby)) {
      client.join(lobby);
      var player = await this.getPlayer(client)
      this.addPLayerToLobby(lobby, player, client.id);
      client.emit('lobby-joined', lobby);
    } else {
      client.emit('connection-denied');
    }
  }

  @SubscribeMessage('leave-lobby')
  async handleLobbyLeave(client: Socket, lobby: string) {
    client.leave(lobby);
    var player = await this.getPlayer(client)
    this.removePLayerFromLobby(lobby, player);
    client.emit('lobby-left', lobby);
  }

  @SubscribeMessage('user-disconnected')
  async handleDisconnection(client: Socket) {
    let player = await this.getPlayer(client)
    let found = false;
    for (let indexLobby = 0; indexLobby < this.lobbies.length; indexLobby++) {
      for (let indexPlayer = 0; indexPlayer < this.lobbies.length; indexPlayer++) {
        if (this.lobbies[indexLobby].players[indexPlayer].name == player) {
          client.leave(indexLobby.toString());
          this.lobbies[indexLobby].players[indexPlayer] = JSON.parse(JSON.stringify(BASE_PLAYER))
          this.removePLayerFromLobby(indexLobby.toString(), player);
          found = true
          break
        }
      }
      if (found) {
        break
      }
    }
  }

  @SubscribeMessage('player-ready')
  async playerIsReady(client: Socket, data: any) {
    data = JSON.parse(data)
    var player = await this.getPlayer(client)
    let lobby = data.lobby
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == player) {
        data.pokemons.forEach(pokemon => {
          pokemon.hpRemaining = pokemon.hp
        });
        this.lobbies[lobby].players[indexPlayer].pokemons = data.pokemons
        this.lobbies[lobby].players[indexPlayer].activePokemon = data.pokemons[0]
        break;
      }
    }
    let allPlayersReady = true
    this.lobbies[lobby].players.forEach(player => {
      if (player.pokemons.length == 0) {
        allPlayersReady = false
      }
    });
    if (allPlayersReady) {
      this.wss.in(data.lobby).emit('all-players-ready');
    }
  }

  async getPlayer(client: Socket): Promise<string> {
    console.log((await this.authService.getUserByToken(client.handshake.headers.authorization)))
    return (await this.authService.getUserByToken(client.handshake.headers.authorization)).pseudo
  }

  @SubscribeMessage('attack')
  async attack(client: Socket, data: any) {
    var player = await this.getPlayer(client)
    var lobby = data.lobby
    // var pokemon = data.pokemon
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == player) {
        this.lobbies[lobby].players[indexPlayer].action.type = Actions.Attack
        this.lobbies[lobby].players[indexPlayer].action.priority = ActionsPriority.Attack
        // this.lobbies[lobby].players[indexPlayer].action.pokemon = pokemon
        this.lobbies[lobby].players[indexPlayer].action.attack = data.attack
      }
    }
    this.resolveTurn(lobby)
  }

  @SubscribeMessage('change-pokemon')
  async changePokemon(client: Socket, data: any) {
    var player = await this.getPlayer(client)
    var lobby = data.lobby
    var pokemon = data.pokemon
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == player) {
        this.lobbies[lobby].players[indexPlayer].action.type = Actions.ChangePokemon
        this.lobbies[lobby].players[indexPlayer].action.priority = ActionsPriority.ChangePokemon
        this.lobbies[lobby].players[indexPlayer].action.pokemon = pokemon
        break
      }
    }
    this.resolveTurn(lobby)
  }

  @SubscribeMessage('run')
  async run(client: Socket, lobby: string) {
    var player = await this.getPlayer(client)
    for (let indexPlayer = 0; indexPlayer < this.lobbies[lobby].players.length; indexPlayer++) {
      if (this.lobbies[lobby].players[indexPlayer].name == player) {
        this.lobbies[lobby].players[indexPlayer].action.type = Actions.Run
        this.lobbies[lobby].players[indexPlayer].action.priority = ActionsPriority.Run
      }
    }
    this.resolveTurn(lobby)
  }

  async insertNewMatchInHistory(lobby, playerOneWon, playerTwoWon){
    let name1 = this.lobbies[lobby].players[0].name
    let name2 = this.lobbies[lobby].players[1].name
    let playerOne = await this.userService.findByPseudo(name1)
    let playerTwo = await this.userService.findByPseudo(name2)

    this.historicService.create({
      createdAt: new Date(),
      idUserOne: playerOne,
      idUserTwo: playerTwo,
      isUserOneVictory: playerOneWon,
      isUserTwoVictory: playerTwoWon
    })
  }

  resolveTurn(lobby) {
    var playerOne = this.lobbies[lobby].players[0]
    var playerTwo = this.lobbies[lobby].players[1]

    if (playerOne.action.type != null && playerTwo.action.type != null) {

      // Run
      if (playerOne.action.type == Actions.Run || playerTwo.action.type == Actions.Run) {
        if (playerOne.action.type == Actions.Run && playerTwo.action.type != Actions.Run) {
          playerOne.data.dialog = ["You left the game, defeat!"]
          this.wss.to(playerOne.socketId).emit('match-ended', playerOne.data);
          playerTwo.data.dialog = ["The oponent left the game, victory!"]
          this.wss.to(playerTwo.socketId).emit('match-ended', playerTwo.data);
          this.insertNewMatchInHistory(lobby, false, true)
        }
        else if (playerOne.action.type != Actions.Run && playerTwo.action.type == Actions.Run) {
          playerOne.data.dialog = ["The oponent left the game, victory!"]
          this.wss.to(playerOne.socketId).emit('match-ended', playerOne.data);
          playerTwo.data.dialog = ["You left the game, defeat!"]
          this.wss.to(playerTwo.socketId).emit('match-ended', playerTwo.data);
          this.insertNewMatchInHistory(lobby, true, false)
        }
        else if (playerOne.action.type == Actions.Run && playerTwo.action.type == Actions.Run) {
          let data = {
            dialog: []
          }
          data.dialog = ["You both left the game, draw game!"]
          this.wss.in(lobby).emit('match-ended', data);
          this.insertNewMatchInHistory(lobby, false, false)
        }
        this.lobbies[lobby] = JSON.parse(JSON.stringify(BASE_LOBBY))
        this.lobbies[lobby] = JSON.parse(JSON.stringify(BASE_LOBBY))
      } else {
        if (playerOne.action.type == playerTwo.action.type) {
          this.resolveSameActions(lobby)
        }
        else if (playerOne.action.priority < playerTwo.action.priority) {
          this.resolveActionsInOrder(lobby, 0, 1)
        }
        else if (playerOne.action.priority > playerTwo.action.priority) {
          this.resolveActionsInOrder(lobby, 1, 0)
        } else {
          playerOne.data.dialog = ["Error, nothing happened."]
          playerTwo.data.dialog = ["Error, nothing happened."]
        }

        if (this.atLeastOnePokemonAlive(lobby, playerOne) && this.atLeastOnePokemonAlive(lobby, playerTwo)) {
          this.lobbies[lobby].players.forEach(player => {
            player.data.lobby = this.getPlayerInfosFromSocketId(lobby, player.socketId)
            this.wss.to(player.socketId).emit('turn-resolved', player.data);
            player.action = JSON.parse(JSON.stringify(BASE_ACTION))
            player.data = JSON.parse(JSON.stringify(BASE_DATA))
          });
        } else {
          if (this.atLeastOnePokemonAlive(lobby, playerOne) && !this.atLeastOnePokemonAlive(lobby, playerTwo)) {
            playerOne.data.dialog = ["The oponent don't have more Pokemons. Victory!"]
            this.wss.to(playerOne.socketId).emit('match-ended', playerOne.data);
            playerTwo.data.dialog = ["Your Pokemon is KO.", "You don't have more Pokemons", "Defeat!"]
            this.wss.to(playerTwo.socketId).emit('match-ended', playerTwo.data);
            this.insertNewMatchInHistory(lobby, true, false)
          } else {
            playerTwo.data.dialog = ["The oponent don't have more Pokemons. Victory!"]
            this.wss.to(playerTwo.socketId).emit('match-ended', playerTwo.data);
            playerOne.data.dialog = ["Your Pokemon is KO.", "You don't have more Pokemons", "Defeat!"]
            this.wss.to(playerOne.socketId).emit('match-ended', playerOne.data);
            this.insertNewMatchInHistory(lobby, false, true)
          }
        }
      }
    }
  }

  atLeastOnePokemonAlive(lobby, player) {
    let found = false
    player.pokemons.forEach(pokemon => {
      if (pokemon.hpRemaining > 0) {
        found = true
      }
    });
    return found
  }

  resolveSameActions(lobby) {
    switch (this.lobbies[lobby].players[0].action.type) {
      case Actions.Attack:
        this.resolveDoubleAttack(lobby)
        break;
      case Actions.ChangePokemon:
        this.resolveActionsInOrder(lobby, 0, 1)
        break;

      default:
        break;
    }
  }

  resolveActionsInOrder(lobby, firstPlayerIndex = 0, secondPlayerIndex = 1) {
    switch (this.lobbies[lobby].players[firstPlayerIndex].action.type) {
      case Actions.ChangePokemon:
        this.resolveChangePokemon(lobby, firstPlayerIndex)
        break;
      case Actions.Attack:
        this.resolveSingleAttack(lobby, firstPlayerIndex, secondPlayerIndex)
        break;

      default:
        break;
    }
    switch (this.lobbies[lobby].players[secondPlayerIndex].action.type) {
      case Actions.ChangePokemon:
        this.resolveChangePokemon(lobby, secondPlayerIndex)
        break;
      case Actions.Attack:
        this.resolveSingleAttack(lobby, secondPlayerIndex, firstPlayerIndex)
        break;

      default:
        break;
    }
  }

  resolveChangePokemon(lobby, playerIndex) {
    this.lobbies[lobby].players[playerIndex].activePokemon = this.lobbies[lobby].players[playerIndex].action.pokemon
  }

  calculDamage(lobby, playerAttackingIndex, playerDefendingIndex) {
    let attack = this.lobbies[lobby].players[playerAttackingIndex].action.attack
    let attackingPokemon = this.lobbies[lobby].players[playerAttackingIndex].activePokemon
    let defendingPokemon = this.lobbies[lobby].players[playerDefendingIndex].activePokemon
    console.log("avant: " + this.lobbies[lobby].players[playerDefendingIndex].activePokemon.hpRemaining)
    // let damage = ((((100 * 0.4 + 2) * attackingPokemon.pointAttack * attack.puissance) / (defendingPokemon.pointDefense * 50) + 2) * 1) // TODO add stabs
    let damage = Math.round( ((((((((2 * 100 /5+2)* attackingPokemon.pointAttack * attack.puissance)/ defendingPokemon.pointDefense)/50)+2)* 1 )* 10 / 10)* 255)/255 )
    // let damage = 20
    // https://github.com/filipekiss/pokemon-type-chart/blob/master/types.json <- Weakness
    let hpRemaining = this.lobbies[lobby].players[playerDefendingIndex].activePokemon.hpRemaining - damage
    this.lobbies[lobby].players[playerDefendingIndex].activePokemon.hpRemaining = hpRemaining > 0 ? hpRemaining : 0
    this.updatePokemonHp(this.lobbies[lobby].players[playerDefendingIndex], defendingPokemon)

    console.log("dommages: " + damage)
    console.log("apres: " + hpRemaining)
    return hpRemaining > 0
  }

  resolveSingleAttack(lobby, playerAttackingIndex, playerDefendingIndex) {
    let playerAttacking = this.lobbies[lobby].players[playerAttackingIndex]
    let playerDefending = this.lobbies[lobby].players[playerDefendingIndex]
    if (this.calculDamage(lobby, playerAttackingIndex, playerDefendingIndex)) {
      playerAttacking.data.dialog = [`You did damages to ${playerDefending.activePokemon.name}`, "What are you going to do?"]
      playerDefending.data.dialog = [`Go ${playerDefending.activePokemon.name}`, "What are you going to do?"]
    } else {
      let newActivePokemon = this.getNewActivePokemon(playerDefending)
      if (newActivePokemon != null) {
        playerDefending.activePokemon = newActivePokemon
        playerDefending.data.dialog = ["Your Pokemon is KO.", `Go ${newActivePokemon.name}`, "What are you going to do?"]
      }
    }
  }

  resolveDoubleAttack(lobby) {
    let playerOne = this.lobbies[lobby].players[0]
    let playerTwo = this.lobbies[lobby].players[1]
    if (playerOne.activePokemon.speed > playerTwo.activePokemon.speed) {
      if (this.calculDamage(lobby, 0, 1)) {
        if(this.calculDamage(lobby, 1, 0)){
          playerOne.data.dialog = [`You did damages to ${playerTwo.activePokemon.name}`, "What are you going to do?"]
          playerTwo.data.dialog = [`You did damages to ${playerOne.activePokemon.name}`, "What are you going to do?"]
        }else{
          let newActivePokemon = this.getNewActivePokemon(playerOne)
          if (newActivePokemon != null) {
            playerOne.activePokemon = newActivePokemon
            playerTwo.data.dialog = [`Oponent's ${playerOne.activePokemon.name} is KO`, "What are you going to do?"]
            playerOne.data.dialog = ["Your Pokemon is KO.", `Go ${newActivePokemon.name}`, "What are you going to do?"]
          }
        }        
      } else {
        let newActivePokemon = this.getNewActivePokemon(playerTwo)
        if (newActivePokemon != null) {
          playerTwo.activePokemon = newActivePokemon
          playerOne.data.dialog = [`Oponent's ${playerTwo.activePokemon.name} is KO`, "What are you going to do?"]
          playerTwo.data.dialog = ["Your Pokemon is KO.", `Go ${newActivePokemon.name}`, "What are you going to do?"]
        }
      }
    }
    else {
      if (this.calculDamage(lobby, 1, 0)) {
        if(this.calculDamage(lobby, 0, 1)){
          playerTwo.data.dialog = [`You did damages to ${playerOne.activePokemon.name}`, "What are you going to do?"]
          playerOne.data.dialog = [`You did damages to ${playerTwo.activePokemon.name}`, "What are you going to do?"]
        }else{
          let newActivePokemon = this.getNewActivePokemon(playerTwo)
          if (newActivePokemon != null) {
            playerTwo.activePokemon = newActivePokemon
            playerTwo.data.dialog = ["Your Pokemon is KO.", `Go ${newActivePokemon.name}`, "What are you going to do?"]
            playerOne.data.dialog = [`Oponent's ${playerTwo.activePokemon.name} is KO`, "What are you going to do?"]
          }
        }

      } else {
        let newActivePokemon = this.getNewActivePokemon(playerOne)
        if (newActivePokemon != null) {
          playerOne.activePokemon = newActivePokemon
          playerOne.data.dialog = ["Your Pokemon is KO.", `Go ${newActivePokemon.name}`, "What are you going to do?"]
          playerTwo.data.dialog = [`Oponent's ${playerOne.activePokemon.name} is KO`, "What are you going to do?"]
        }
      }
    }
  }

  updatePokemonHp(player, newPokemon){
    let pokemonIndex = player.pokemons.findIndex( pokemon => pokemon.id == newPokemon.id)
    player.pokemons[pokemonIndex] = newPokemon
  }

  getNewActivePokemon(player) {
    let pokemon = null
    for (let indexPokemon = 0; indexPokemon < player.pokemons.length; indexPokemon++) {
      if (player.pokemons[indexPokemon].hpRemaining > 0) {
        pokemon = player.pokemons[indexPokemon]
        break
      }
    }
    return pokemon
  }

  @SubscribeMessage('get-lobby-informations')
  async getLobbyInformations(client: Socket, lobby: string) {
    var lobbyInfos = {
      myPokemons: [],
      foePokemon: null
    }
    var player = await this.getPlayer(client)
    if (this.lobbies[lobby].players[0].name == player) {
      lobbyInfos.myPokemons = this.lobbies[lobby].players[0].pokemons
      lobbyInfos.foePokemon = this.lobbies[lobby].players[1].activePokemon
    } else {
      lobbyInfos.myPokemons = this.lobbies[lobby].players[1].pokemons
      lobbyInfos.foePokemon = this.lobbies[lobby].players[0].activePokemon
    }
    client.emit('lobby-informations', lobbyInfos);
  }

  getPlayerInfosFromSocketId(lobby, socketId) {
    var lobbyInfos = {
      pokemons: [],
      activePokemon: null,
      foePokemon: null
    }
    if (this.lobbies[lobby].players[0].socketId == socketId) {
      lobbyInfos.pokemons = this.lobbies[lobby].players[0].pokemons
      lobbyInfos.activePokemon = this.lobbies[lobby].players[0].activePokemon
      lobbyInfos.foePokemon = this.lobbies[lobby].players[1].activePokemon
    } else {
      lobbyInfos.pokemons = this.lobbies[lobby].players[1].pokemons
      lobbyInfos.activePokemon = this.lobbies[lobby].players[1].activePokemon
      lobbyInfos.foePokemon = this.lobbies[lobby].players[0].activePokemon
    }
    return lobbyInfos
  }


  // 
}

