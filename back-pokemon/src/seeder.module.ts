
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserModule } from "./user/user.module";
import { AuthModule } from "./auth/auth.modules";
import { TypePokemonModule } from "./typePokemon/typePokemon.module";
import { PokemonModule } from "./pokemon/pokemon.module";
import { CapacityModule } from "./capacity/capacity.module";
import { Module, Logger } from "@nestjs/common";
import { Seeder } from "./seeder";

@Module({
    imports: [
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'postgres',
          password: 'root',
          database: 'pokemon',
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
      }), UserModule, AuthModule, TypePokemonModule, PokemonModule, CapacityModule,
    ],
    providers: [ Logger, Seeder],
  
})
export class SeederModule {}