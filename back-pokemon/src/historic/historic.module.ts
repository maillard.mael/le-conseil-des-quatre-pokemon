import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Historic} from "./historic.entity";
import {HistoricService} from "./historic.service";
import {HistoricController} from "./historic.controller";

@Module({
    imports: [TypeOrmModule.forFeature([Historic])],
    providers: [HistoricService],
    controllers: [HistoricController],
    exports: [HistoricService]
})
export class HistoricModule {}
