import {ApiTags} from "@nestjs/swagger";
import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Req, UseGuards} from "@nestjs/common";
import {HistoricService} from "./historic.service";
import {CreateHistoricDto} from "./dto/create-historic.dto";
import {Historic} from "./historic.entity";
import {UpdateHistoricDto} from "./dto/update-historic.dto";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";

@ApiTags('historic')
@Controller('historic')
export class HistoricController {
    constructor(private readonly historicService: HistoricService) {}

    @UseGuards(JwtAuthGuard)
    @Get('me')
    async findByUserId(@Req() req): Promise<Historic[]> {
        const historic = await this.historicService.findByUserId(req.user.userId);
        if (historic === undefined) {
            throw new NotFoundException('Historic not found')
        }
        return historic;
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<Historic> {
        const historic = await this.historicService.findOne(id);
        if (historic === undefined) {
            throw new NotFoundException('Historic not found')
        }
        return historic;
    }

    @Get()
    findAll(): Promise<Historic[]> {
        return this.historicService.findAll();
    }

    @Post()
    create(@Body() createHistoricDto: CreateHistoricDto): Promise<Historic> {
        return this.historicService.create(createHistoricDto);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() updateHistoricDto: UpdateHistoricDto): Promise<any> {
        const historic = await this.historicService.findOne(id);
        if (historic === undefined) {
            throw new NotFoundException('Historic not found')
        } else {
            return await this.historicService.update(id ,updateHistoricDto);
        }
    }

    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
        return this.historicService.remove(id);
    }
}
