import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Historic} from "./historic.entity";
import {Repository, UpdateResult} from "typeorm";
import {CreateHistoricDto} from "./dto/create-historic.dto";
import {UpdateHistoricDto} from "./dto/update-historic.dto";

@Injectable()
export class HistoricService {
    constructor(
        @InjectRepository(Historic)
        private readonly historicRepository: Repository<Historic>,
    ) {}

    findAll(): Promise<Historic[]> {
        return this.historicRepository.find();
    }

    async findByUserId(userId: string): Promise<Historic[]> {
        return await this.historicRepository.createQueryBuilder('historic')
            .where('historic.idUserOne = :userId', {userId})
            .orWhere('historic.idUserTwo = :userId', {userId})
            .leftJoinAndSelect('historic.idUserOne', 'userOne')
            .leftJoinAndSelect('historic.idUserTwo', 'userTwo')
            .orderBy('historic.createdAt', 'DESC')
            .getMany();
    }

    findOne(id: string): Promise<Historic> {
        return this.historicRepository.findOne(id);
    }

    create(createHistoricDto: CreateHistoricDto): Promise<Historic> {
        return this.historicRepository.save(createHistoricDto);
    }

    async update(id: string, updateHistoricDto: UpdateHistoricDto): Promise<UpdateResult> {
        return await this.historicRepository.update(id, updateHistoricDto);
    }

    async remove(id: string): Promise<void> {
        await this.historicRepository.delete(id);
    }
}
