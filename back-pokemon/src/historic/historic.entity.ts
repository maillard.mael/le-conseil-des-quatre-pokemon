import {
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    Column
} from "typeorm";
import {User} from "../user/user.entity";

@Entity()
export class Historic {
    @PrimaryGeneratedColumn()
    id: number;
    @ManyToOne('User', 'historic', {eager: false})
    @JoinColumn({name: 'idUserOne', referencedColumnName: 'id'})
    idUserOne: User;
    @ManyToOne('User', 'historic', {eager: false})
    @JoinColumn({name: 'idUserTwo', referencedColumnName: 'id'})
    idUserTwo: User;
    @Column()
    isUserOneVictory: Boolean;
    @Column()
    isUserTwoVictory: Boolean;
    @CreateDateColumn()
    createdAt: Date;
}
