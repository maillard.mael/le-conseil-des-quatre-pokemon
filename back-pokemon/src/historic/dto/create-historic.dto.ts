import { User } from "../../user/user.entity";
import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
// import {CustomizePokemon} from "../../customizePokemon/customizePokemon.entity";
import { Pokemon } from "../../pokemon/pokemon.entity";

export class CreateHistoricDto {
    @ApiProperty({ type: String })
    @IsNotEmpty()
    idUserOne: User;
    @ApiProperty({ type: String })
    @IsNotEmpty()
    idUserTwo: User;
    @ApiProperty({ type: Boolean })
    @IsNotEmpty()
    isUserOneVictory: Boolean;
    @ApiProperty({ type: Boolean })
    @IsNotEmpty()
    isUserTwoVictory: Boolean;
    @ApiProperty({ type: Date })
    @IsNotEmpty()
    createdAt: Date;
}
