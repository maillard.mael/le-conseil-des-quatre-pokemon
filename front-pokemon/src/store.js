import Vue from 'vue'
import Vuex from 'vuex'
import config from './client.conf'
import AuthService from './services/auth.service'
import axios from 'axios'

Vue.use(Vuex)
const $authService = new AuthService(config.base_url, axios);
export default new Vuex.Store({
  state: {
    status: '',
    token: localStorage.getItem('token') || ''
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token, user){
      state.status = 'success',
      state.token = token
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = '',
      state.token = ''
    },
  },
  actions: {
    login({commit}, req){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        $authService.login(req.username, req.password,
          resp=>{
            const token = resp.data.access_token
            localStorage.setItem('token', token)
            localStorage.setItem('id_user', resp.data.id)
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
            commit('auth_success', token)

            resolve()
          },
          err => {
            commit('auth_error')
            localStorage.removeItem('token')
            reject(err)
          }
        );
      })
    },

    logout({commit}){
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token');
        localStorage.removeItem('id_user');
        delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    },

  },
  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status
  }
})
