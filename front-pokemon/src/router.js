import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Arena from './views/Arena.vue'
import Informations from './views/Informations.vue'
import Historic from './views/Historic.vue'
import Register from './views/Register.vue'
import Pokemon from './views/Pokemon.vue'
import OnePokemon from './views/OnePokemon.vue'
import ArenaPokemonSelection from './views/ArenaPokemonSelection.vue'
import ArenaFight from './views/ArenaFight.vue'
import store from './store'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: { auth: true }
    },
    {
      path: "/arena",
      name: "arena",
      component: Arena,
      meta: { auth: true }
    },
    {
      path: "/arena/:id/selection",
      name: "arena-pokemon-selection",
      component: ArenaPokemonSelection,
      meta: { auth: true }
    },
    {
      path: "/arena/:id/fight",
      name: "arena-fight",
      component: ArenaFight,
      meta: { auth: true }
    },
    {
      path: "/informations",
      name: "informations",
      component: Informations,
      meta: { auth: true }
    },
    {
      path: "/historic",
      name: "historic",
      component: Historic,
      meta: { auth: true }
    },
    {
      path: "/pokemon",
      name: "pokemon",
      component: Pokemon,
      meta: { auth: true }
    },
    {
      path: "/pokemon/:id",
      name: "one-pokemon",
      component: OnePokemon,
      meta: { auth: true }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: { auth: false }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: { auth: false }
    },
  ]
});


router.beforeEach((to, from, next) => {
  var isLoggedIn = store.getters.isLoggedIn;
  if (to.meta.auth != undefined) {
    if (!isLoggedIn && to.meta.auth) {
      next({
        path: "/login"
      });
    } else if (isLoggedIn && !to.meta.auth) {
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
