import Service from './service'
export default class AuthService extends Service{
    table = 'auth';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

    login(username, password, then, error){
        this.$http.post(this.base_url + this.table + '/login',{
            "username": username,
            "password": password
        }
        )
        .then(response =>{
            then(response);
        })
        .catch(msg => {
            error("Login failed, check your username and password")
        });
    }
}