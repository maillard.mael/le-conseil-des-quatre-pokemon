import Service from './service'
export default class LobbyService extends Service{
    table = 'lobby';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

}