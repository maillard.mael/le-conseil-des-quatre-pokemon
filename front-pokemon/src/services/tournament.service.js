import Service from './service'
export default class TournamentService extends Service{
    table = 'tournament';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

}