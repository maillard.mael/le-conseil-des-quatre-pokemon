export default class Service{
    table = 'name';
    base_url = null;
    $http = null;

    constructor(base_url, $http){
        this.base_url = base_url;
        this.$http = $http;
    }

    find(id, then, error){
        this.$http.get(this.base_url + this.table + '/' + id)
        .then(response =>{
            then(response.data)
        })
        .catch(msg =>{
            console.error(msg);
            error(msg)
        });
    }

    findAll(then, error){
        this.$http.get(this.base_url + this.table)
        .then(response =>{ then(response.data)})
        .catch(msg => error(msg));
    }

    create(data, then, error){
        this.$http.post(this.base_url + this.table, data)
        .then(response =>{ then(response.data)})
        .catch(msg => error(msg));
    }

    update(data, then, error){
        this.$http.put(this.base_url + this.table, data)
        .then(response =>{ then(response.data)})
        .catch(msg => error(msg));
    }

    remove(data, then, error){
        this.$http.delete(this.base_url + this.table, data)
        .then(response =>{ then(response.data)})
        .catch(msg => error(msg));
    }
}