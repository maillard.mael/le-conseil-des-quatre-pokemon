import Service from './service'
export default class CapacityService extends Service{
    table1 = 'pokemonTypes';
    table2 = 'capacities';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

    getCapacitiesByType(id, then, error) {
        this.$http.get(this.base_url + this.table1 + '/'+ id + '/'+ this.table2)
          .then(response => {
              then(response);
          })
          .catch(msg => {
              error(msg)
          });
    }

    getAllCapacities(then, error) {
      this.$http.get(this.base_url + this.table2)
        .then(response => {
          then(response);
        })
        .catch(msg => {
          error(msg)
        });
    }
}
