import VueSocketIO from 'vue-socket.io'
import SocketIO from "socket.io-client"

export default class SocketService {

    socket = null;

    constructor(){ 
        // this.socket = new VueSocketIO({
        //     debug: true,
        //     connection: SocketIO('http://localhost:3000/masterServer', {
        //       autoConnect: false,  
        //       transportOptions: {
        //         polling: {
        //           extraHeaders: {
        //               'authorization': 'Bearer ' + localStorage.getItem('token')
        //           }
        //         }
        //       }
        //     })
        // })
    }

    openSocket(){
        console.log('socket openning...')
        this.socket.io.open();
        console.log('socket openned')
    }

    closeSocket(){
        console.log('socket closing...')
        this.socket.io.close();
        console.log('socket closed')
    }

    getAllLobbies(){
        console.log('getAllLobbies')
        console.log(this.socket)
        console.log(this.socket.io)
        this.socket.io.on('LobbyList', data => console.log(data));
        this.socket.io.emit('getAllLtestsobbies');       

    }
}