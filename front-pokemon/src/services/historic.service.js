import Service from './service'
export default class HistoricService extends Service{
    table = 'historic';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

    getMeHistoric(then, error) {
        this.$http.get(this.base_url + this.table + '/me')
            .then(response => {
                then(response);
            })
            .catch(msg => {
                error(msg)
            });
    }

}