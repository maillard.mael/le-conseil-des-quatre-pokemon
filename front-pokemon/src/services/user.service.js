import Service from './service'
export default class UserService extends Service {
    table = 'user';

    constructor(base_url, Axios) {
        super(base_url, Axios);
    }

    register(username, password, then, error) {
        this.$http.post(this.base_url + this.table, {
            "pseudo": username,
            "password": password
        }
        )
            .then(response => {
                then(response);
            })
            .catch(msg => {
                error("Register failed")
            });
    }

    informations(then, error) {
        this.$http.get(this.base_url + this.table + '/me')
            .then(response => {
                then(response);
            })
            .catch(msg => {
                error(msg)
            });
    }

    inventory(then, error) {
        this.$http.get(this.base_url + this.table + '/inventory')
          .then(response => {
              then(response);
          })
          .catch(msg => {
              error(msg)
          });
    }
}
