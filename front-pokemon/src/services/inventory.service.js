import Service from './service'
export default class InventoryService extends Service{
    table = 'userInventories';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

    addPokemonToInventory(body, then, error) {
        this.$http.post(this.base_url + this.table, body)
          .then(response => {
              then(response);
          })
          .catch(msg => {
              error(msg)
          });
    }
}
