import Service from './service'
export default class PokemonService extends Service{
    table1 = 'pokemons';
    table2 = 'customizePokemon';

    constructor(base_url, Axios){
        super(base_url, Axios);
    }

    getCustomPokemon(id, then, error) {
        this.$http.get(this.base_url + this.table2 + '/'+ id)
          .then(response => {
              then(response);
          })
          .catch(msg => {
              error(msg)
          });
    }

    getRandomPokemon( then, error) {
      this.$http.get(this.base_url + this.table1 + '/random')
        .then(response => {
          then(response);
        })
        .catch(msg => {
          error(msg)
        });
    }

    createCustomPokemon(body, then, error) {
      this.$http.post(this.base_url + this.table2, body)
        .then(response => {
          then(response);
        })
        .catch(msg => {
          error(msg)
        });
    }

    updateCustomPokemon(id, body, then, error) {
      this.$http.put(this.base_url + this.table2 + '/'+ id, body
      )
        .then(response => {
          then(response);
        })
        .catch(msg => {
          error("Creation failed: " + msg)
        });
    }

}
