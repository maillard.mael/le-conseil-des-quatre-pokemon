import config from './client.conf'

import Vue from 'vue'
import App from './App.vue'
import './../node_modules/bulma/css/bulma.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFistRaised, faShoppingBag, faRunning, faList, faUser, faKey, faCheck, faInfoCircle, faBackward, faHistory, faRegistered, faEdit, faSave, faExclamationCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'
import Axios from 'axios'
import store from './store'

import io from "socket.io-client"


// import 'bootstrap'; 
// import 'bootstrap/dist/css/bootstrap.min.css';

import AuthService from './services/auth.service'
import UserService from './services/user.service'
import CapacityService from './services/capacity.service'
import HistoricService from './services/historic.service'
import LobbyService from './services/lobby.service'
import PokemonService from './services/pokemon.service'
import InventoryService from './services/inventory.service'
import TournamentService from './services/tournament.service'
import SocketService from './services/socket.service'

library.add(faFistRaised, faShoppingBag, faRunning, faList, faUser, faKey, faCheck, faInfoCircle, faBackward, faHistory, faRegistered, faEdit, faSave, faExclamationCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

//Services
Vue.prototype.$authService = new AuthService(config.base_url, Axios);
Vue.prototype.$userService = new UserService(config.base_url, Axios);
Vue.prototype.$capacityService = new CapacityService(config.base_url, Axios);
Vue.prototype.$historicService = new HistoricService(config.base_url, Axios);
Vue.prototype.$lobbyService = new LobbyService(config.base_url, Axios);
Vue.prototype.$pokemonService = new PokemonService(config.base_url, Axios);
Vue.prototype.$inventoryService = new InventoryService(config.base_url, Axios);
Vue.prototype.$tournamentService = new TournamentService(config.base_url, Axios);

Vue.prototype.$http = Axios;

//Authentication token
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

Vue.prototype.$initSocket = () => {

  Vue.prototype.$socket = io('http://localhost:3000/masterServer', {
    transportOptions: {
      polling: {
        extraHeaders: {
            'authorization': 'Bearer ' + localStorage.getItem('token')
        }
      }
    }
  })
}

Vue.prototype.$initSocket();

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
