export default class WriteTextBox {
    constructor() {
        this.timeouts = []
        this.runAfter = null
    }

    /**
     * Run typing script
     * @param {string[]} texts Array of texts, render by ordr
     */
    run(texts = []) {
        if (!texts.length) return

        this.destroy()

        document.querySelector('.text-box p').textContent = ""

        for (const [index, letter] of texts[0].split('').entries()) {
            this.timeouts.push(
                setTimeout(
                    () => {
                        document.querySelector('.text-box p').textContent += letter //Add new letter
                        if ((index + 1 === texts[0].split('').length) && texts.length > 1) {  //Display second text
                            this.runAfter = setTimeout(() => this.run(texts.slice(1)), 1000)
                        }
                    },
                    35 * (index + 1)
                )
            )
        }
    }

    /**
     * Deny typing script
     */
    destroy() {
        this.timeouts.forEach(x => clearTimeout(x))
        clearTimeout(this.runAfter)
        this.timeouts = []
    }
}