CREATE TABLE "user" (
  "id" SERIAL PRIMARY KEY,
  "pseudo" varchar,
  "password" varchar,
  "created_at" timestamp,
  "upload_at" timestamp
);

CREATE TABLE "inventoryUser" (
  "id" SERIAL PRIMARY KEY,
  "idUser" int,
  "idPokemon" int,
  "idCapacity" int,
  "idCapacity2" int
);

CREATE TABLE "tournois" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "date" timestamp,
  "json" json
);

CREATE TABLE "pokemon" (
  "id" SERIAL PRIMARY KEY,
  "numero" int,
  "name" varchar,
  "pointAttack" int,
  "pointDefense" int,
  "spirtImage" varchar,
  "idTypePokemon" int
);

CREATE TABLE "typePokemon" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "capacity" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "idType" int,
  "puissance" int,
  "precision" int
);

CREATE TABLE "combat" (
  "id" SERIAL PRIMARY KEY,
  "idUser1" int,
  "idUser2" int,
  "victory" int,
  "date" timestamp
);

ALTER TABLE "inventoryUser" ADD FOREIGN KEY ("idUser") REFERENCES "user" ("id");

ALTER TABLE "inventoryUser" ADD FOREIGN KEY ("idPokemon") REFERENCES "pokemon" ("id");

ALTER TABLE "inventoryUser" ADD FOREIGN KEY ("idCapacity") REFERENCES "capacity" ("id");

ALTER TABLE "inventoryUser" ADD FOREIGN KEY ("idCapacity2") REFERENCES "capacity" ("id");

ALTER TABLE "pokemon" ADD FOREIGN KEY ("idTypePokemon") REFERENCES "typePokemon" ("id");

ALTER TABLE "capacity" ADD FOREIGN KEY ("idType") REFERENCES "typePokemon" ("id");

ALTER TABLE "combat" ADD FOREIGN KEY ("idUser1") REFERENCES "user" ("id");

ALTER TABLE "combat" ADD FOREIGN KEY ("idUser2") REFERENCES "user" ("id");
